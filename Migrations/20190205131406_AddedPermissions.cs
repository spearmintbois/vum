﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Spearmint.Vum.Backend.Api.Migrations
{
    public partial class AddedPermissions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { 2, "Access to administrator features", "admin" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "BaseRoleId", "Description", "Name" },
                values: new object[] { 2, null, "Admin boi", "Administrator" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "Email" },
                values: new object[] { new DateTime(2019, 2, 5, 14, 14, 5, 203, DateTimeKind.Local).AddTicks(6136), "aaron@vum.li" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "Email" },
                values: new object[] { new DateTime(2019, 2, 5, 14, 14, 5, 209, DateTimeKind.Local).AddTicks(284), "vito@vum.li" });

            migrationBuilder.CreateIndex(
                name: "IX_RolePermissions_PermissionId",
                table: "RolePermissions",
                column: "PermissionId");

            migrationBuilder.AddForeignKey(
                name: "FK_RolePermissions_Permissions_PermissionId",
                table: "RolePermissions",
                column: "PermissionId",
                principalTable: "Permissions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RolePermissions_Permissions_PermissionId",
                table: "RolePermissions");

            migrationBuilder.DropIndex(
                name: "IX_RolePermissions_PermissionId",
                table: "RolePermissions");

            migrationBuilder.DeleteData(
                table: "Permissions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "Email" },
                values: new object[] { new DateTime(2019, 1, 22, 14, 56, 5, 540, DateTimeKind.Local).AddTicks(6404), "aaron.schmid@outlook.com" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "Email" },
                values: new object[] { new DateTime(2019, 1, 22, 14, 56, 5, 544, DateTimeKind.Local).AddTicks(2223), "vito.melchionna@outlook.com" });
        }
    }
}
