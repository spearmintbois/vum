FROM node AS frontend-build-env
WORKDIR /app

COPY ./Client ./
RUN npm install

# Build via webpack
RUN npm run build:prod

FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

ARG SEED_DB

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

ENV ASPNETCORE_ENVIRONMENT production

# Copy everything else
COPY . ./
COPY --from=frontend-build-env /app/dist ./wwwroot/js

# Seed the database
RUN if [ $SEED_DB == 'yes' ]; then dotnet ef database update; fi

# Build
RUN dotnet publish -c Release -o out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime

WORKDIR /app
COPY --from=build-env /app/out .

EXPOSE 80

COPY entrypoint.sh .
RUN chmod +x ./entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]

