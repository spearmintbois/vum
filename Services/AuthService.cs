using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using Spearmint.Vum.Backend.Api.Helpers;
using Spearmint.Vum.Backend.Api.Models;
using Spearmint.Vum.Backend.Api.Models.DTOs;
using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Api.Services
{
    public interface IAuthService
    {
        AuthResult Authenticate(string username, string password);
        Model.Entities.User GetUserFromPrincipal(ClaimsPrincipal user);
    }

    public class AuthService : IAuthService
    {
        private IRepositoryService repository;
        private AppSettings appSettings;

        public AuthService(IRepositoryService repository, IOptions<AppSettings> appSettings)
        {
            this.repository = repository;
            this.appSettings = appSettings.Value;
        }

        public AuthResult Authenticate(string username, string password)
        {
            var user = repository.UserRepository.Find(username, password);
            if(user == null) {
                return new AuthResult {
                    Success = false
                };
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            
            return new AuthResult {
                Success = true,
                User = new Models.DTOs.User(user, true),
                Token = tokenHandler.WriteToken(token)
            };
        }

        public Model.Entities.User GetUserFromPrincipal(ClaimsPrincipal user)
        {
            var identity = user.Identity as ClaimsIdentity;
            var userId = identity.FindFirst(ClaimTypes.Name)?.Value;
            if(userId == null) {
                return null;
            }
            return repository.UserRepository.Get(int.Parse(userId));
        }

        public bool UserHasPermission(String permission)
        {
            return false;
        }
    }
}