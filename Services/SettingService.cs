namespace Spearmint.Vum.Backend.Api.Services
{
    public interface ISettingService {
        int RoleId { get; set; }
    }

    public class SettingService : ISettingService
    {
        private readonly IRepositoryService repository;

        public SettingService(IRepositoryService repository) {
            this.repository = repository;
        }

        public int RoleId {
            get {
                return int.Parse(this.repository.SettingRepository.GetSetting("DefaultRoleId"));
            }
            set {
                this.repository.SettingRepository.UpdateOrCreateSetting("DefaultRoleId", value.ToString());
            }
        }
    }
}