using Spearmint.Vum.Backend.Model;
using Spearmint.Vum.Backend.Model.Repositories;

namespace Spearmint.Vum.Backend.Api.Services
{
    public interface IRepositoryService
    {
        VumContext Context{ get; }
        UserRepository UserRepository { get; }
        PostRepository PostRepository { get; }
        SettingRepository SettingRepository { get; }
        VoteRepository VoteRepository { get; }
        CommentRepository CommentRepository { get; }
    }

    public class RepositoryService : IRepositoryService
    {
        private VumContext context;
        private UserRepository userRepository;
        private PostRepository postRepository;
        private SettingRepository settingRepository;
        private VoteRepository voteRepository;
        private CommentRepository commentRepository;

        public VumContext Context {
            get {
                return this.context;
            }
        }

        public UserRepository UserRepository {
            get {
                return this.userRepository;
            }
        }

        public PostRepository PostRepository {
            get {
                return this.postRepository;
            }
        }

        public SettingRepository SettingRepository {
            get {
                return this.settingRepository;
            }
        }

        public VoteRepository VoteRepository {
            get {
                return this.voteRepository;
            }
        }

        public CommentRepository CommentRepository {
            get {
                return this.commentRepository;
            }
        }

        public RepositoryService(VumContext context) {
            this.context = context;
            this.userRepository = new UserRepository(context);
            this.postRepository = new PostRepository(context);
            this.settingRepository = new SettingRepository(context);
            this.voteRepository = new VoteRepository(context);
            this.commentRepository = new CommentRepository(context);
        }
    }
}