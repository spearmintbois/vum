using System;

using Spearmint.Vum.Backend.Api.Events;

namespace Spearmint.Vum.Backend.Api.Services
{
    public interface IEventService
    {
        event EventHandler<NewPostEvent> NewPost;
        void RaiseNewPostEvent(object sender, NewPostEvent args);
    }
    public class EventService : IEventService
    {
        public event EventHandler<NewPostEvent> NewPost;

        public void RaiseNewPostEvent(object sender, NewPostEvent args)
        {
            this.NewPost?.Invoke(sender, args);
        }
    }
}