using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Spearmint.Vum.Backend.Api.Middlewares
{
    public class PermissionMiddleware
    {
        private readonly RequestDelegate next;

        public PermissionMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            await next.Invoke(context);
        }
    }
}