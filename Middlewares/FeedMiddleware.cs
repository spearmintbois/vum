using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

using Spearmint.Vum.Backend.Api.Events;
using Spearmint.Vum.Backend.Api.Services;

namespace Spearmint.Vum.Backend.Api.Middlewares
{
    public class FeedMiddleware
    {
        private static ConcurrentDictionary<string, WebSocket> sockets = new ConcurrentDictionary<string, WebSocket>();
 
        private readonly RequestDelegate next;
        private readonly IEventService eventService;
    
        public FeedMiddleware(RequestDelegate next, IEventService eventService)
        {
            this.next = next;
            this.eventService = eventService;
            this.eventService.NewPost += OnNewPost;
        }
    
        public void OnNewPost(object sender, NewPostEvent args) {
            BroadcastString(JsonConvert.SerializeObject(new Models.DTOs.Post(args.Post)));
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest && context.Request.Path != "/api/feed")
            {
                await next.Invoke(context);
                return;
            }
    
            CancellationToken ct = context.RequestAborted;
            WebSocket currentSocket = await context.WebSockets.AcceptWebSocketAsync();
            var socketId = Guid.NewGuid().ToString();
    
            sockets.TryAdd(socketId, currentSocket);

            while (true)
            {
                if (ct.IsCancellationRequested)
                {
                    break;
                }
                if(currentSocket.State != WebSocketState.Open)
                {
                    break;
                }
            }
    
            WebSocket dummy;
            sockets.TryRemove(socketId, out dummy);
    
            await currentSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", ct);
            currentSocket.Dispose();
        }

        private static Task BroadcastString(string data, CancellationToken ct = default(CancellationToken)) {
            var sendTasks = new List<Task>();
            foreach (var socket in sockets)
            {
                if(socket.Value.State != WebSocketState.Open)
                {
                    continue;
                }
                sendTasks.Add(SendStringAsync(socket.Value, data, ct));
            }
            return Task.WhenAll(sendTasks);
        }
    
        private static Task SendStringAsync(WebSocket socket, string data, CancellationToken ct = default(CancellationToken))
        {
            var buffer = Encoding.UTF8.GetBytes(data);
            var segment = new ArraySegment<byte>(buffer);
            return socket.SendAsync(segment, WebSocketMessageType.Text, true, ct);
        }
    }
}