using System;
using System.Linq;
using System.Collections.Generic;

namespace Spearmint.Vum.Backend.Api.Models.DTOs
{
    public class Comment
    {
        public Comment() {

        }

        public Comment(Model.Entities.Comment comment) {
            this.Id = comment.Id;
            this.Content = comment.Content;
            this.Poster = new User(comment.Poster);
            this.PostedAt = comment.PostedAt;

            if(comment.Replies != null) {
                this.Replies = comment.Replies.Select(c => new Comment(c)).ToList();
            }
        }

        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime PostedAt { get; set; }

        public User Poster { get; set; }

        public List<Comment> Replies { get; set; }
    }
}