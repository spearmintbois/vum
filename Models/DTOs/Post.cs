namespace Spearmint.Vum.Backend.Api.Models.DTOs
{
    public class Post
    {
        public Post() {

        }

        public Post(Model.Entities.Post post) {
            this.Id = post.Id;
            this.Title = post.Title;
            this.Poster = new User(post.Poster);
            this.PosterName = post.Poster.Username;

            if(post.Attachment != null) {
                this.HasAttachment = true;
                this.AttachmentUrl = "/api/post/" + post.Id + "/attachment";
            } else {
                this.HasAttachment = false;
            }
            
            if(post.Content != null) {
                this.HasContent = true;
                this.Content = post.Content;
            } else {
                this.HasContent = false;
            }

            this.VoteBalance = post.VoteBalance;
            this.UpVotes = post.UpVotes;
            this.DownVotes = post.DownVotes;
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string AttachmentUrl { get; set; }
        public bool HasContent { get; set; }
        public bool HasAttachment { get; set; }
        public string PosterName { get; set; }
        public User Poster { get; set; }
        public int VoteBalance { get; set; }
        public int UpVotes { get; set; }
        public int DownVotes { get; set; }
    }
}