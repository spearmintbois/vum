using System;

namespace Spearmint.Vum.Backend.Api.Models.DTOs
{
    public class User
    {
        public User()
        {

        }

        public User(Spearmint.Vum.Backend.Model.Entities.User user)
        {
            Username = user.Username;
            Created = user.Created;
            Active = user.Active;
        }

        public User(Spearmint.Vum.Backend.Model.Entities.User user, bool withEmail) : this(user) {
            if(withEmail) {
                Email = user.Email;
            }
        }

        public string Username { get; set; }
        public string Email { get; set; }
        public DateTime Created { get; set; }
        public bool Active { get; set; }
    }
}