using System;
using System.Linq;

using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Model.Repositories
{
    public class VoteRepository : BaseRepository<Vote>
    {
        public VoteRepository(VumContext context) : base(context)
        {
            
        }

        public bool UserVotedOnPost(User user, Post post) {
            return context.Votes.SingleOrDefault(v => v.PostId == post.Id && v.UserId == user.Id) != null;
        }

        public void UpVotePost(Post post, User user) {
            if(UserVotedOnPost(user, post)) {
                RemoveVoteFromUser(post, user);
            }
            context.Votes.Add(new Vote() {
                type = VoteType.Upvote,
                PostId = post.Id,
                UserId = user.Id
            });
            post.UpVotes++;
            post.VoteBalance++;
            context.SaveChanges();
        }

        public void DownVotePost(Post post, User user) {
            if(UserVotedOnPost(user, post)) {
                RemoveVoteFromUser(post, user);
            }            
            context.Votes.Add(new Vote() {
                type = VoteType.Downvote,
                PostId = post.Id,
                UserId = user.Id
            });
            post.DownVotes++;
            post.VoteBalance--;
            context.SaveChanges();
        }

        public void RemoveVoteFromUser(Post post, User user) {
            Vote vote = context.Votes.SingleOrDefault(v => v.PostId == post.Id && v.UserId == user.Id);
            if(vote == null) {
                return;
            }

            context.Votes.Remove(vote);
            if(vote.type == VoteType.Upvote) {
                post.UpVotes--;
                post.VoteBalance--;
            } else {
                post.DownVotes--;
                post.VoteBalance++;
            }
            context.SaveChanges();
        }
    }
}