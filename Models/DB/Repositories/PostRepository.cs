using System;

using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Model.Repositories
{
    public class PostRepository : BaseRepository<Post>
    {
        public PostRepository(VumContext context) : base(context)
        {
            
        }

        public Post CreatePost(Post post) {
            if(post.Attachment != null) {
                if(post.AttachmentFilename == null || post.AttachmentMIMEType == null) {
                    return null;
                }
            } else if(post.Content == null) {
                return null;
            }
            post.PostedAt = DateTime.Now;
            return this.Create(post);
        }

        public bool DeletePost(int postId) {
            if(postId > 0) {
                this.Delete(postId);
                return true;
            } else {
                return false;
            }
        }
    }
}