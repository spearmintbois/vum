using System;
using System.Collections.Generic;
using System.Linq;

using Spearmint.Vum.Backend.Model;
using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Model.Repositories
{
    public class BaseRepository<T> where T : EntityBase
    {
        protected VumContext context;

        public BaseRepository(VumContext context) {
            this.context = context;
        }

        public T Get(int id) {
            return context.Set<T>().SingleOrDefault(r => r.Id == id);
        }

        public T Create(T entity) {
            try {
                context.Set<T>().Add(entity);
                context.SaveChanges();
            }
            catch(Exception ex) {
                Console.WriteLine(ex.InnerException);
                throw ex;
            }
            return entity;
        }

        public void Delete(int id) {
            var entity = Get(id);
            
            if (entity != null)
            {
                context.Set<T>().Remove(entity);
                context.SaveChanges();
            }
        }

        public IEnumerable<T> GetAll() {
            return context.Set<T>();
        }
    }
}