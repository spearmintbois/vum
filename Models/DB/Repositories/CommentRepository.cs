using System;

using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Model.Repositories
{
    public class CommentRepository : BaseRepository<Comment>
    {
        public CommentRepository(VumContext context) : base(context)
        {
            
        }
    }
}