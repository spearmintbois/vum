using System;
using System.Linq;

using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Model.Repositories
{
    public class SettingRepository : BaseRepository<Setting>
    {
        public SettingRepository(VumContext context) : base(context)
        {
            
        }

        public void UpdateOrCreateSetting(string name, string value) {
            var setting = context.Settings.SingleOrDefault(s => s.Name.Equals(name));
            if(setting == null) {
                context.Settings.Add(new Setting {
                    Name = name,
                    Value = value
                });
            } else {
                setting.Value = value;
                context.Settings.Update(setting);
            }
        }

        public string GetSetting(string name) {
            return context.Settings.SingleOrDefault(s => s.Name.Equals(name))?.Value;
        }
    }
}