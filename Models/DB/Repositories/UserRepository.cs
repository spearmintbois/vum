using System;
using System.Linq;

using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Model.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(VumContext context) : base(context) {

        }

        public User CreateUser(string username, string email, string password, int roleId) {
            if(context.Users.ToList().Exists(u => u.Username == username) || context.Users.ToList().Exists(u => u.Email == email)) {
                return null;
            }

            return Create(new User() {
                Username = username,
                Email = email,
                Password = password,
                Active = true,
                Created = DateTime.Now,
                RoleId = roleId
            });
        }

        public User Find(string username, string password) {
            return context.Users.SingleOrDefault(r => r.Username.Equals(username) && r.Password.Equals(password));
        }

        public User Find(string username) {
            return context.Users.SingleOrDefault(r => r.Username.Equals(username));
        }
    }
}