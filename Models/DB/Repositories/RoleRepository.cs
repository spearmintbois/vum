using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Model.Repositories
{
    public class RoleRepository : BaseRepository<Role>
    {
        public RoleRepository(VumContext context) : base(context) {

        }

        
    }
}