using System.ComponentModel.DataAnnotations;

namespace Spearmint.Vum.Backend.Model.Entities
{
    public class Setting : EntityBase
    {
        [Required]
        public string Name { get; set; }
        public string Value { get; set; }
    }
}