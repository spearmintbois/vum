using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Spearmint.Vum.Backend.Model.Entities
{
    public class Comment : EntityBase
    {
        [Required]
        public string Content { get; set; }
        [Required]
        public DateTime PostedAt { get; set; }

        [Required]
        public int PosterId { get; set; }
        public virtual User Poster { get; set; }

        [Required]
        public int PostId { get; set; }
        public virtual Post Post { get; set; }

        public int? ReplyToId { get; set; }
        public virtual Comment ReplyTo { get; set; }
        public virtual List<Comment> Replies { get; set; }
    }
}