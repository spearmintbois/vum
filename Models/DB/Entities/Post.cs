using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Spearmint.Vum.Backend.Model.Entities
{
    public class Post : EntityBase
    {
        [Required]
        public string Title { get; set; }
        public byte[] Attachment { get; set; }
        public string AttachmentMIMEType { get; set; }
        public string AttachmentFilename { get; set; }
        public string Content { get; set; }
        public bool NSFW { get; set; }
        public DateTime PostedAt { get; set; }

        public int VoteBalance { get; set; }
        public int UpVotes { get; set; }
        public int DownVotes { get; set; }

        public int PosterId { get; set; }
        public virtual User Poster { get; set; }

        public virtual List<Comment> Comments { get; set; }
    }
}