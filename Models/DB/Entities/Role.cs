using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Spearmint.Vum.Backend.Model.Entities
{
    public class Role : EntityBase
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual List<User> Users { get; set; }

        public int? BaseRoleId { get; set; }
        public virtual Role BaseRole { get; set; }

        public virtual List<RolePermission> RolePermissions { get; set; }
    }
}