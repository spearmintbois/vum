using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Spearmint.Vum.Backend.Model.Entities
{
    public class Permission : EntityBase
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<RolePermission> RolePermission { get; set; }
    }
}