using System.ComponentModel.DataAnnotations;

namespace Spearmint.Vum.Backend.Model.Entities
{
    public class RolePermission : EntityBase
    {
        [Required]
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }
        
        [Required]
        public int PermissionId { get; set; }
        public virtual Permission Permission { get; set; }
    }
}