using System.ComponentModel.DataAnnotations;

namespace Spearmint.Vum.Backend.Model.Entities
{
    public enum VoteType {
        Upvote,
        Downvote
    }

    public class Vote : EntityBase
    {
        public VoteType type { get; set; }

        [Required]
        public int UserId { get; set; }
        virtual public User User { get; set; }

        [Required]
        public int PostId { get; set; }
        virtual public Post Post { get; set; }
    }
}