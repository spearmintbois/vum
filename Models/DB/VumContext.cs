using System.Linq;
using Microsoft.EntityFrameworkCore;

using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Model
{
    public class VumContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Vote> Votes { get; set; }

        public VumContext(DbContextOptions<VumContext> options) : base(options) {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder ) {
            modelBuilder.Entity<Vote>()
                .HasIndex(p => new { p.UserId, p.PostId })
                .IsUnique();
            modelBuilder.Entity<Post>()
                .Property(p => p.VoteBalance)
                .HasDefaultValue(0);
            modelBuilder.Entity<Post>()
                .Property(p => p.UpVotes)
                .HasDefaultValue(0);
            modelBuilder.Entity<Post>()
                .Property(p => p.DownVotes)
                .HasDefaultValue(0);

            modelBuilder.Entity<Setting>().HasData(new Setting {
                Id = 1,
                Name = "DefaultRoleId",
                Value = "1"
            });

            modelBuilder.Entity<Permission>().HasData(new Permission {
                Id = 1,
                Name = "Test",
                Description = "Test Permission, does nothing..."
            }, new Permission {
                Id = 2,
                Name = "admin",
                Description = "Access to administrator features"
            });
            
            modelBuilder.Entity<Role>().HasData(new Role {
                Id = 1,
                Name = "User",
                Description = "Normal users"
            }, new Role {
                Id = 2,
                Name = "Administrator",
                Description = "Admin boi"
            });

            modelBuilder.Entity<RolePermission>().HasData(new RolePermission {
                Id = 1,
                PermissionId = 1,
                RoleId = 1
            });

			modelBuilder.Entity<User>().HasData(new User {
                Id = 1,
                Username = "aaron",
                Email = "aaron@vum.li",
                Password = "123456",
                Active = true,
                Created = System.DateTime.Now,
                RoleId = 1
            }, new User {
                Id = 2,
                Username = "vito",
                Email = "vito@vum.li",
                Password = "123456",
                Active = true,
                Created = System.DateTime.Now,
                RoleId = 1
            });
        }
    }
}