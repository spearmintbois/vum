using Microsoft.IdentityModel.Tokens;

using Spearmint.Vum.Backend.Api.Models.DTOs;

namespace Spearmint.Vum.Backend.Api.Models
{
    public class AuthResult
    {
        public bool Success { get; set; }
        public User User { get; set; }
        public string Token { get; set; }
    }
}