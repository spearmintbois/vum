namespace Spearmint.Vum.Backend.Api.Models
{
    public class VoteResponse
    {
        public int VoteBalance { get; set; }
        public int UpVotes { get; set; }
        public int DownVotes { get; set; }
    }
}