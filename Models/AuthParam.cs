namespace Spearmint.Vum.Backend.Api.Models
{
    public class AuthParam
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}