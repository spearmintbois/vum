using System.ComponentModel.DataAnnotations;

namespace Spearmint.Vum.Backend.Api.Models
{
    public class PostUploadParam
    {
        [Required]
        public string Title { get; set; }
        public string Content { get; set; }
    }
}