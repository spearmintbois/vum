using System.ComponentModel.DataAnnotations;

namespace Spearmint.Vum.Backend.Api.Models
{
    public class RegisterParam
    {
        [Required]
        public string Username;
        [Required]
        public string Password;
        [Required]
        public string Email;
    }
}