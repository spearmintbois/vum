using System.ComponentModel.DataAnnotations;

namespace Spearmint.Vum.Backend.Api.Models
{
    public class UserUpdateParam
    {
        public string Password;
        public string NewPassword;
        public string Email;
    }
}