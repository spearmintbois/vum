namespace Spearmint.Vum.Backend.Api.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string[] MIMETypeWhitelist { get; set; }
    }
}