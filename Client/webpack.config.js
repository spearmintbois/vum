const resolve = require('path').resolve;
const merge = require('webpack-merge');

module.exports = (env) => {
    return merge({
        entry: resolve('src'),
        output: {
            path: resolve('dist'),
            filename: 'bundle.js',
            library: 'vum'
        },
        module: {
            rules: [{
                test: /\.tsx?$/,
                use: [
                    'ts-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader', 'css-loader'
                ]
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader', 'css-loader', 'less-loader'
                ]
            }]
        },
        resolve: {
            extensions: ['.jsx', '.tsx', '.js', '.ts', '.css', '.less'],
            descriptionFiles: ['package.json'],
            modules: [resolve('node_modules'), 'node_modules']
        }
    }, require('./webpack.' + env + '.js'));
} 