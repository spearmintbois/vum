import * as React from 'react';
import { Backend } from '../backend';

type AvatarProps = {
    user : string
    showName? : boolean
    height? : string
    width? : string
};

export const Avatar : React.StatelessComponent<AvatarProps> = (props) => {
    return <div>
        <img className="rounded-circle" height={ props.height == null ? "50" : props.height } width={ props.width == null ? "50" : props.width } src={ Backend.getAvatarUrl(props.user) } />
        { props.showName ? <span className="text-nowrap"><small>{ props.user }</small></span> : null }
    </div>;
};
