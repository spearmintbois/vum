import * as React from 'react';
import { AppContext } from '../model/appContext';
import { Post } from '../model/post';
import { Backend } from '../backend';
import { Socket } from '../socket';
import { AdminPostComponent } from './adminPost';
import { AdminUsersOverviewComponent } from './adminUsersOverview';
import { User } from '../model/user';

type AdminDashboardProps = {
    context : AppContext
}

type AdminDashboardState = {
    posts : Post[]
    users : User[]
}

export class AdminDashboard extends React.Component<AdminDashboardProps, AdminDashboardState> {
    private socket : Socket;

    constructor(props) {
        super(props);

        this.state = {
            posts: [],
            users: []
        }; 

        this.handleDeletePost = this.handleDeletePost.bind(this);

        this.socket = new Socket();
        this.socket.onNewPost = (post) => {
            this.handleNewPost(post);
        };
    }

    componentDidMount() {
        Backend.getPosts().then((posts) => {
            this.setState({
                posts: posts
            })
        });

        Backend.getUsers().then((users) => {
            this.setState({
                users: users
            })
        });
    }

    componentDidUpdate() {
        Backend.getPosts().then((posts) => {
            this.setState({
                posts: posts
            })
        });
    }

    handleNewPost(post : Post) {
        this.setState({
            posts: [post].concat(this.state.posts)
        });
    }

    handleDeletePost(postId : number){

        Backend.deletePost(postId).then((promise) => {
            if(promise != undefined){
                
                /*var newposts = this.state.posts.map((post) => {
                    if(post.id != postId) {
                        return post;
                    }
                });
                this.setState(){
                    posts: newposts
                }
                */

                this.componentDidUpdate();
            }           
        });
    }

    handleDeleteUser(userName : String){

        Backend.deleteUser(userName).then((promise) => {
            if(promise != undefined){

                this.componentDidUpdate();
            }           
        });
    }

    render() {
        return <div className="container" id="feed">
            <div className="row">
                { this.state.posts.map(p => <AdminPostComponent key={ p.id } context={ this.props.context } post={ p } handleDeletePost={ this.handleDeletePost } />) }
                { this.state.users.map(u => <AdminUsersOverviewComponent context={ this.props.context } user={ u } handleDeleteUser={ this.handleDeleteUser } />) }
            </div>
        </div>;
    }
}