import * as React from 'react';
import { AppContext } from '../model/appContext';
import { Notification } from '../model/notification';   

type NotificationProps = {
    context : AppContext
    notification : Notification
    onClose : () => void
}

export const NotificationComponent : React.StatelessComponent<NotificationProps> = (props) => {
    return <div className={ "alert alert-dismissible " + props.notification.type }>
        <a className="close" aria-label="close" onClick={ () => props.onClose() }>&times;</a>
        { props.notification.content }
    </div>;
};
