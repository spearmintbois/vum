import * as React from 'react';
import { AppContext } from '../model/appContext';
import { Post } from '../model/post';
import { Avatar } from './avatar';

type AdminPostProps = {
    context : AppContext
    post : Post
    handleDeletePost : (postId) => void
}

export const AdminPostComponent : React.StatelessComponent<AdminPostProps> = (props) => {

    return <div className="post col-9">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="d-flex justify-content-between">
                                <div className="col-2">
                                    <Avatar user={ props.post.posterName } showName={ true } />
                                </div>
                                <div>
                                    <div className="btn"><h4>{props.post.voteBalance}</h4></div>
                                    <button 
                                        onClick={() => props.handleDeletePost(props.post.id)} 
                                        type="button" 
                                        className="btn btn-danger">
                                        X
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <h3>{ props.post.title }</h3>
                    </div>           
                    <div className="row">
                        { props.post.hasAttachment ? <img className="img-fluid post-attachment" src={ props.post.attachmentUrl } /> : null }                            
                    </div>
                    <div className="row">
                        { props.post.hasContent ? <p>{ props.post.content }</p> : null }    
                    </div>
                </div>
            </div>;
}
