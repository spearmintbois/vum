import * as React from 'react';
import { History } from 'history';
import { withRouter } from "react-router";

import { AppContext } from '../model/appContext';
import { NavigationLoggedIn } from './navigationLoggedIn';
import { HeaderLoginAction } from './headerLoginAction';

type HeaderProps = {
    context : AppContext
    history : History
}

const HeaderC : React.StatelessComponent<HeaderProps> = (props) => {
    return <nav className="navbar fixed-top navbar-dark bg-dark navbar-expand-lg">
        <a className="navbar-brand mr-auto" onClick={ () => props.history.push({ pathname: '/' }) }><img src="assets/vum.png" height="40" /> <small>- Where the memes stay fresh 🔥👾💯</small></a>
        { props.context.isLoggedIn ? <NavigationLoggedIn context = { props.context } /> : <HeaderLoginAction context={ props.context } /> }
    </nav>;
}

export const Header = withRouter(HeaderC);