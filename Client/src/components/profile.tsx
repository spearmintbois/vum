import * as React from 'react';

import { AppContext } from '../model/appContext';
import { User } from '../model/user';
import { Post } from '../model/post';
import { Avatar } from './avatar';
import { ReactionPanel } from './reactionPanel';
import { PostComponent } from './post';
import { Backend } from '../backend';

type ProfileProps = {
    context : AppContext
    name : string
}

type ProfileState = {
    posts : Array<Post>
}

export class Profile extends React.Component<ProfileProps, ProfileState> {
    constructor(props) {
        super(props);

        this.state = {
            posts: []
        };
    }

    componentWillMount() {
        Backend.getPostsFrom(this.props.name).then((posts) => {
            this.setState({
                posts: posts
            });
        }).catch(this.props.context.notificationManagerRef.current.defaultHTTPCatch);
    }

    render() {
        var likes = 0, dislikes = 0;
        this.state.posts.forEach((post) => {
            likes += post.upVotes;
            dislikes += post.downVotes;
        });

        return <div id="profile" className="container">
            <div className="row">
                <div className="col">
                    <div className="d-flex justify-content-between">
                        <div className="col-2">
                            <Avatar user={ this.props.name } showName={ true } />
                        </div>
                        <div>
                            <ReactionPanel upvotes={ likes } downvotes={ dislikes } />
                        </div>
                    </div>
                </div>
                <div className="col">
                    <h2>{ this.props.name } Post's</h2>
                </div>
                <div className="col" id="feed">
                    { this.state.posts.map(p => <PostComponent key={ p.id } context={ this.props.context } post={ p } handleVotes={ () => alert("Liking is currently only supported from the front page :(") } />) }
                </div>
            </div>
        </div>;
    }

}
