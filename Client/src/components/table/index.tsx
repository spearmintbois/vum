import { fetchAccessor } from '../../util';

import './table.less';

export { Table } from './table';
export { CellProps } from './tableBodyColumn';

export type Column = {
    header: string
    default?: string
    accessor?: string
    getValue?: (row: any) => string
    cell?: (props) => JSX.Element
    
    // Styling
    className? : string | ((row: any) => string)
    width?: string
    height?: string

    // Sort
    sortable?: boolean
    sort?: (a, b) => 1 | -1 | 0 
}

export type Filter = {
    text?: string
    default?: any
    filter: (value: any, onChange: (value: any) => void) => JSX.Element
    filterMethod: (filter, row) => boolean
}

export class TableContext {
    data?: Array<any>
    columns: Array<Column>
    filters?: Array<Filter>

    emptyDataText: string

    // Sort
    sortable: boolean = true
    sortColumn?: Column
    sortDirection?: SortDirection
    sortCallback: (column : number) => void

    // Pagination
    pagination: boolean = false
    currentPage: number = 0
    totalPages: number = 1
    pagedData?: Array<any>
    paginationCallback: (page: number) => void

    // Filter
    filterValues: FilterValues
    filterOnChange: (filter: number, newValue: any) => void
}

export interface FilterValues {
    [filter: number] : any;
}

export function GetValue(column : Column, row : any) : string {
    let value = row;
    
    if(column.accessor != null) {
        value = fetchAccessor(row, column.accessor);
    }
    if(column.getValue != null) {
        value = column.getValue(value);
    }

    if(value == null) {
        value = column.default || '';
    }
    if(typeof value === "object") {
        value = "object";
    }

    return value;
}

export enum SortDirection {
    ASC = -1,
    DESC = 1
}
