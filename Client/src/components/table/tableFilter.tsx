import * as React from 'react';
import { TableContext } from '.';

type TableFilterProps = {
    context : TableContext
};

export const TableFilter : React.StatelessComponent<TableFilterProps> = (props) => {
    
    function generateFilter(filter, index) {
        let value = null;
        if(props.context.filterValues != null) {
            value = props.context.filterValues[index];
        }
        if(filter.text != null) {
            return <div className="col-4 filter-field" key={ index }>
                <div className="row form-group">
                    <label className="col-sm-4 col-form-label col-form-label-sm">{ filter.text }</label>
                    <div className="col-sm-8">
                        { filter.filter(value, (value: any) => { onChangeCallback(index, value) }) }
                    </div>
                </div>
            </div>;
        }
        return <div className="col-4 filter-field" key={ index }>
            { filter.filter(value, (value: any) => { onChangeCallback(index, value) }) }
        </div>;
    }

    let onChangeCallback = (filter: number, value: any) => {
        props.context.filterOnChange(filter, value);
    };

    let rows = [],
        filters = props.context.filters;
    for(let i = 0; i < Math.ceil(filters.length / 3); i++) {
        let row = [];
        for(let j = i * 3; j < i * 3 + 3; j++) {
            if(j in filters) {
                row.push(filters[j]);
            }
        }
        rows.push(row);
    }

    return <div className="filter">
        <form>
            <fieldset className="filter-fieldset">
                <legend className="filter-fieldset-legend">Filter</legend>
                    <div className="container">
                        { rows.map((r, j) => <div className="row" key={ j }>
                            { r.map((filter, i) => {
                                let ix = j * 3 + i;
                                return generateFilter(filter, ix) } ) }
                        </div>) }
                    </div>
            </fieldset>
        </form>
    </div>;
};

