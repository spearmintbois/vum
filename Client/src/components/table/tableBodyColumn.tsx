import * as React from 'react';
import { TableContext, GetValue } from '.';
import { Column } from '.';
import { ErrorBoundary } from '../errorBoundary';

type TableBodyColumnProps = {
    context: TableContext
    column: Column
    row: any
};

export type CellProps = {
    context: TableContext
    column: Column
    row: any
    value: string
};

export const TableBodyColumn : React.StatelessComponent<TableBodyColumnProps> = (props) => {
    let value = GetValue(props.column, props.row),
        cell = <span>{ value }</span>;

    if(props.column.cell != null) {
        cell = <ErrorBoundary>
            { React.createElement(props.column.cell, { context: props.context, column: props.column, row: props.row, value: value }) }
        </ErrorBoundary>;
    }

    let className = props.column.className;
    if(typeof(className) === "function") {
        className = className(props.row);
    }

    let styling = {
        height: props.column.height,
        width: props.column.width
    };

    return <td className={ (className || "") + " bodyColumn" } style={ styling }>
        { cell }
    </td>;
};
