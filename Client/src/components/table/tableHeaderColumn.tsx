import * as React from 'react';
import { TableContext, Column } from '.';
import { TableSortDirection } from './tableSortDirection';

export type TableHeaderColumnProps = {
    id : number
    context : TableContext
    column : Column
};

export const TableHeaderColumn : React.StatelessComponent<TableHeaderColumnProps> = (props) => {
    let styling = {
        height: props.column.height,
        width: props.column.width
    };

    return <th onClick={ () => props.context.sortCallback(props.id) } style={ styling } className="headerColumn">
        <div className="headerCell container">
            <span>{ props.column.header }</span><TableSortDirection context={ props.context } column={ props.column } />
        </div>
    </th>;
};
