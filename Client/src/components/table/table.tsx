import * as React from 'react';
import { Column, SortDirection, TableContext, FilterValues, GetValue, Filter } from '.';
import { TableHeader } from './tableHeader';
import { TableBody } from './tableBody';
import { TableFilter } from './tableFilter';
import { TablePagination } from './tablePagination';
import { Loading } from '../loading';

type TableProp = {
    data?: Array<any>
    columns: Array<Column>
    filters?: Array<Filter>

    emptyDataText?: string

    // Sort
    sortable?: boolean

    // Pagination
    pagination?: boolean
    rowsPerPage?: number
}

type TableState = {
    data?: Array<any>

    // Sort
    sortByColumn? : number
    sortDirection? : SortDirection

    // Pagination
    currentPage: number

    // Filter
    filterValues?: FilterValues
}

const sort = (r1, r2) => r1 == r2 ? 0 : r1 < r2 ? 1 : -1;

export class Table extends React.Component<TableProp, TableState> {
    constructor(props) {
        super(props);

        let filterValues = {};
        if(props.filters != null) {
            props.filters.forEach((filter, index) => {
                if(filter.default != null) {
                    filterValues[index] = filter.default;
                }
            });
        }        

        this.state = {
            data: null,
            sortByColumn: null,
            sortDirection: null,
            currentPage: 0,
            filterValues: filterValues
        };

        this.sort = this.sort.bind(this);
        this.updatePage = this.updatePage.bind(this);
        this.filterOnUpdate = this.filterOnUpdate.bind(this);
    }

    /**
     * Generates Context Class, for passing to children
     */
    getContext() : TableContext {
        var ctx = new TableContext();
        ctx.data = this.getData();
        ctx.columns = this.props.columns;

        ctx.emptyDataText = this.props.emptyDataText || 'No Entries!';

        // Sort
        ctx.sortable = this.props.sortable || ctx.sortable; // TODO: Implement sortable field
        ctx.sortCallback = this.sort;
        ctx.sortColumn = ctx.columns[this.state.sortByColumn];
        ctx.sortDirection = this.state.sortDirection;

        // Pagination
        ctx.pagination = this.props.pagination || ctx.pagination;
        if(ctx.pagination) {
            ctx.paginationCallback = this.updatePage;
            ctx.currentPage = this.state.currentPage;
            if(ctx.data != null) {
                ctx.pagedData = this.applyPagination(ctx.data);
                ctx.totalPages = Math.ceil(ctx.data.length / (this.props.rowsPerPage || 10));
            }
        }

        // Filter
        ctx.filterOnChange = this.filterOnUpdate;
        ctx.filterValues = this.state.filterValues;
        ctx.filters = this.props.filters;

        return ctx;
    }

    /**
     * Sets state to sort by specific column
     * @param column index of column in column array
     */
    sort(column : number) {
        var currSortColumn = this.state.sortByColumn,
            newSortColumn = column,
            newSortDirection = SortDirection.ASC;

        if(currSortColumn != null && currSortColumn == column) {
            // Change sort direction
            newSortDirection = this.state.sortDirection * -1;
        }

        this.setState({
            sortByColumn: newSortColumn,
            sortDirection: newSortDirection
        });
    }

    /**
     * Updates the state for chaning the current page number
     * @param page New page number
     */
    updatePage(page : number) {
        this.setState({
            currentPage: page
        });
    }

    filterOnUpdate(filter: number, value: any) {
        let n = this.state.filterValues || {};
        n[filter] = value;

        let prefilter = this.applyFilters(this.props.data, n),
            currPage = this.state.currentPage,
            totalPages = Math.ceil(prefilter.length / (this.props.rowsPerPage || 10));

        if(totalPages < currPage) {
            currPage = totalPages - 1;
        }

        this.setState({
            filterValues: n,
            currentPage: currPage
        });
    }

    getData() : Array<any> {
        let data = this.props.data;
        if(data == null) {
            return null;
        }
        data = this.applyFilters(data);
        if(this.props.sortable && this.state.sortByColumn != null) {
            data = this.applySort(data);
        }
        return data;
    }

    applySort(data: Array<any>) : Array<any> {
        let column = this.props.columns[this.state.sortByColumn],
            direction = this.state.sortDirection;
        return data.sort((a, b) => {
            if(column.sort != null) {
                return direction * column.sort(a, b);
            }
            return direction * sort(GetValue(column, a).toLocaleLowerCase(), GetValue(column, b).toLocaleLowerCase());
        });
    }

    applyFilters(data: Array<any>, filters: FilterValues = this.state.filterValues) : Array<any> {
        return data.filter((r) => {
            if(filters != null) {
                for(let i in filters) {
                    if(!this.props.filters[i].filterMethod(filters[i], r)) {
                        return false;
                    }
                }
            }
            return true;
        });
    }

    applyPagination(data: Array<any>) : Array<any> {
        let rowsPerPage = this.props.rowsPerPage || 10,
            pageMin = rowsPerPage * this.state.currentPage,
            pageMax = pageMin + rowsPerPage;
        return data.filter((r, i) => {
            if(i >= pageMin && i < pageMax) {
                return true;
            }
            return false;
        });
    }

    render() {
        var ctx = this.getContext();
        return <div className="sxtable table-responsive">
            { ctx.filters ? <TableFilter context={ ctx } /> : null }
            <table className="table">
                <TableHeader context={ ctx } />
                { ctx.data != null ? <TableBody context={ ctx } /> : null }
            </table>
            { ctx.data != null ? null : <Loading className="mx-auto d-block" /> }
            <TablePagination context={ ctx } />
        </div>;
    }
}
