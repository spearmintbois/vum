import * as React from 'react';
import { TableContext } from '.';
import { TableBodyRow } from './tableBodyRow';

type TableBodyProps = {
    context : TableContext
};

export const TableBody : React.StatelessComponent<TableBodyProps> = (props) => {
    let ctx = props.context;
    let data = ctx.pagination ? ctx.pagedData : ctx.data;

    return <tbody>
        { data.map((row, i) => <TableBodyRow context={ props.context } row={ row } key={ i } />) }
        { data.length == 0 ? <tr><td><p>{ props.context.emptyDataText }</p></td></tr> : null }
    </tbody>;
};
