import * as React from 'react';
import { TableContext, Column, SortDirection } from '.';

type TableSortDirectionProps = {
    context: TableContext
    column: Column
};

export const TableSortDirection : React.StatelessComponent<TableSortDirectionProps> = (props) => {
    if (props.context.sortColumn != null && props.context.sortColumn.header == props.column.header) {
        if(props.context.sortDirection == SortDirection.ASC) {
            return <span> ▲</span>;
        } else if(props.context.sortDirection == SortDirection.DESC) {
            return <span> ▼</span>;
        }
    }    
    return null;
};
