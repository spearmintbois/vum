import * as React from 'react';
import { TableContext } from '.';
import { TableHeaderColumn } from './tableHeaderColumn';

type TableHeaderProps = {
    context : TableContext
};

export const TableHeader : React.StatelessComponent<TableHeaderProps> = (props) => {
    return <thead>
        <tr className="headerRow">
            { props.context.columns.map((column, i) => <TableHeaderColumn context={ props.context } column={ column } key={ i } id={ i } />) }
        </tr>
    </thead>;
};
