import * as React from 'react';
import { TableContext } from '.';
import { TableBodyColumn } from './tableBodyColumn';

type TableBodyRowProps = {
    context : TableContext
    row : any
};

export const TableBodyRow : React.StatelessComponent<TableBodyRowProps> = (props) => {
    return <tr className="bodyRow">
        { props.context.columns.map((column, i) => <TableBodyColumn context={ props.context } column={ column } row={ props.row } key={ i } />) }
    </tr>;
};
