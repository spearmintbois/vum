import * as React from 'react';
import { TableContext } from '.';

type TablePaginationProp = {
    context: TableContext
}

export class TablePagination extends React.Component<TablePaginationProp, any> {
    constructor(props) {
        super(props);
        this.handlePerv = this.handlePerv.bind(this);
        this.handleNext = this.handleNext.bind(this);
    }

    handlePerv() {
        this.props.context.paginationCallback(this.props.context.currentPage - 1);
    }

    handleNext() {
        this.props.context.paginationCallback(this.props.context.currentPage + 1);
    }

    render() {
        let ctx = this.props.context;
        if(ctx.data == null || ctx.data.length == 0 || ctx.totalPages <= 1) {
            return null;
        }

        let pagination = [];
        for(let i = 0; i < ctx.totalPages; i++) {
            pagination.push(<li key={ i } 
                                onClick={ () => this.props.context.paginationCallback(i) } 
                                className={ this.props.context.currentPage == i ? "pagination-pages-list-page current" : "pagination-pages-list-page" }>
                                { i + 1 }</li>);
        }

        let hasPrev = this.props.context.currentPage != 0,
            hasNext = ctx.totalPages - 1 > this.props.context.currentPage;

        return <div className="pagination">
            { hasPrev ? <i className="pagination-nav prev" onClick={ event => this.handlePerv() } /> : <i className="pagination-nav prev hidden" /> }
            <div className="pagination-pages">
                <ul className="pagination-pages-list">
                    { pagination }
                </ul>
            </div>
            { hasNext ? <i className="pagination-nav next" onClick={ event => this.handleNext() } /> : <i className="pagination-nav next hidden" /> }
        </div>;
    }
}
