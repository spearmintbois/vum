import * as React from 'react';
import { Link } from 'react-router-dom';

import { AppContext } from '../model/appContext';

type NavigationLoggedInProps = {
    context : AppContext
}

export const NavigationLoggedIn : React.StatelessComponent<NavigationLoggedInProps> = (props) => {

    const adminComponentLink = props.context.isAdmin ? 
                    <li className="nav-item">
                        <div className="btn btn-header btn-admin">
                            <Link to="/admin">Admin</Link>
                        </div> 
                    </li> : "";

    return <ul className="nav navbar-nav">
        {adminComponentLink}
        <li className="nav-item">
            <div className="btn btn-header btn-profile">
                <Link to={ "/profile/" + props.context.loggedInUser.username }>{ props.context.loggedInUser.username }</Link>
            </div>
        </li>
        <li className="nav-item">
            <div className="btn btn-header btn-settings">
                <Link to="/settings"><img src="assets/icons/settings.svg" className="img-fluid" width="24" /></Link>
            </div>
        </li>
        <li className="nav-item">
            <div className="btn btn-header btn-post">
                <Link to="/post"><img src="assets/icons/write.svg" className="img-fluid" width="24" /></Link>
            </div>
        </li>        
    </ul>;
};
 