import * as React from 'react';
import { History } from 'history';
import { withRouter } from "react-router";

import { Backend } from '../backend';
import { AppContext } from '../model/appContext';
import { NotificationType } from '../model/notification';

type PostingPopupProps = {
    context : AppContext
    history : History
}

type PostingPopupState = {
    postTitle : string,
    postFile : File,
    postContent : string
}

class Post extends React.Component<PostingPopupProps, PostingPopupState> {
    constructor(props) {
        super(props);

        this.state = {
            postTitle: '',
            postFile : null,
            postContent: ''
        }

        this.handleClose = this.handleClose.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleOnChangeTitle = this.handleOnChangeTitle.bind(this);
        this.handleOnChangeContent = this.handleOnChangeContent.bind(this);
        this.handleOnChangePostFile = this.handleOnChangePostFile.bind(this);
    }

    handleClose() {
        this.props.history.push({
            pathname: '/'
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        event.stopPropagation();

        Backend.uploadPost(this.state.postTitle, this.state.postContent, this.state.postFile).then(() => {
            this.props.context.notificationManagerRef.current.addNotification({
                type: NotificationType.success,
                content: <div>Uploaded you'r post, you rock! 🤟</div>
            });
            this.handleClose();
        }).catch(this.props.context.notificationManagerRef.current.defaultHTTPCatch);
    }

    handleOnChangeTitle(event) {
        this.setState({
            postTitle: event.target.value
        });
    }

    handleOnChangeContent(event) {
        this.setState({
            postContent: event.target.value
        });
    }

    handleOnChangePostFile(event) {
        this.setState({
            postFile: event.target.files[0]
        });
    }

    render() {
        return <form onSubmit={ this.handleSubmit }>
            <div className="modal fade show" role="dialog" style={{ display: 'block' }}>
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Add a new post! ❤</h5>
                        </div>
                        <div className="modal-body">
                            <p>
                                Got a cool meme? Show it the world! 😁 But remember: everything that you upload will belong to us and therefore will remain forever in the Internet. Please don't be mad at us. Google and Facebook do the same; they just never tell you 😇. Have fun! 😉 
                            </p>
                            <div className="form-group">
                                <input type="text" className="form-control" placeholder="Your post's title" onChange={ this.handleOnChangeTitle } required />
                                {
                                    this.state.postTitle == "" ? <div className="invalid-feedback" style={{ display: 'block' }}>
                                    Your post need a title, you smart *ss motherf***er 🤓
                                    </div> : null
                                }
                            </div>
                            <div className="form-group">
                                <input type="text" className="form-control" placeholder="Your post's content" onChange={ this.handleOnChangeContent } />
                            </div>
                            <div className="form-group">
                                <input type="file" name="post" accept="image/*" className="form-control" onChange={ this.handleOnChangePostFile } />
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={ this.handleClose }>Cancel</button>
                            <button type="submit" className="btn btn-primary btn-block">Post your post!</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal-backdrop show" />
        </form>;
    }
}

export const PostPopup = withRouter(Post);
