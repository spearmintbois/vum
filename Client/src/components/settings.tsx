import * as React from 'react';
import { History } from 'history';
import { withRouter } from "react-router";

import { AppContext } from '../model/appContext';
import { User } from '../model/user';
import { Backend } from '../backend';
import { NotificationType } from '../model/notification';


type SettingsProps = {
    context : AppContext
    history : History
}

type SettingsState = {
    email : string
    currentPassword? : string
    newPassword? : string
    avatar? : File
}


class SettingsC extends React.Component<SettingsProps, SettingsState> {

    constructor(props) {
        super(props);

        this.state = {
            email: this.props.context.loggedInUser.email
        };

        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.handleOnAvatarChange = this.handleOnAvatarChange.bind(this);
        this.handleOnEmailChange = this.handleOnEmailChange.bind(this);
        this.handleOnCurrentPasswordChange = this.handleOnCurrentPasswordChange.bind(this);
        this.handleOnNewPasswordChange = this.handleOnNewPasswordChange.bind(this);
    }

    handleOnSubmit(event) {
        event.preventDefault();
        event.stopPropagation();

        if(this.state.avatar != null) {
            Backend.updateUserAvatar(this.state.avatar).then(() => {
                this.props.context.notificationManagerRef.current.addNotification({
                    type: NotificationType.success,
                    content: <div>Looks great amigo 👍</div>
                });
            }).catch(this.props.context.notificationManagerRef.current.defaultHTTPCatch);
        }

        Backend.updateUser(this.state.email, this.state.currentPassword, this.state.newPassword).then(() => {
            this.props.context.notificationManagerRef.current.addNotification({
                type: NotificationType.success,
                content: <div>Updated you'r shissel ☝</div>
            });
        }).catch(this.props.context.notificationManagerRef.current.defaultHTTPCatch);
    }
    
    handleOnAvatarChange(e) {
        this.setState({
            avatar: e.target.files[0]
        });
    }

    handleOnEmailChange(e) {
        this.setState({
            email: e.target.value
        });
    }

    handleOnCurrentPasswordChange(e) {
        this.setState({
            currentPassword: e.target.value
        });
    }

    handleOnNewPasswordChange(e) {
        this.setState({
            newPassword: e.target.value
        });
    }

    render() {
        return <form onSubmit={ this.handleOnSubmit }>
            <div className="container" id="settings">
                <div className="row">
                    <h1>Account settings</h1>
                </div>
                <div className="row">
                    <h2>Public settings</h2>
                </div>
                <div className="row">
                    <div className="form-group">
                        <label>Avatar 🤠</label>
                        <input className="form-control" type="file" onChange={ this.handleOnAvatarChange } />
                        <small className="form-text text-muted">Please don't upload nasty images to our server, thank's! 😅</small>
                    </div>
                </div>
                <div className="row">
                    <h2>Private settings</h2>
                </div>
                <div className="row">
                    <p>These will be never shared with other users. Only we and you can see them.</p>
                </div>
                <div className="row">
                    <div className="form-group">
                        <label>E-Mail</label>
                        <input className="form-control" type="email" onChange={ this.handleOnEmailChange } value={ this.state.email } />
                    </div>
                </div>
                <div className="row">
                    <h2>Change Password</h2>
                </div>
                <div className="row">
                    <div className="form-group">
                        <label>Current password</label>
                        <input className="form-control" type="password" onChange={ this.handleOnCurrentPasswordChange } value={ this.state.currentPassword } />
                    </div>
                </div>
                <div className="row">
                    <div className="form-group">
                        <label>New Password</label>
                        <input className="form-control" type="password" onChange={ this.handleOnNewPasswordChange } value={ this.state.newPassword } />
                    </div>
                </div>
                <div className="row d-flex justify-content-between">
                    <button className="btn btn-secondary" type="reset" onClick={ () => this.props.history.push({ pathname: '/' }) }>About</button>
                    <button className="btn btn-submit" type="submit">Update</button>
                </div>
            </div>
        </form>;
    }
}

export const Settings = withRouter(SettingsC);
