import * as React from 'react';
import { AppContext } from '../model/appContext';
import { NotificationComponent } from './notification';
import { Notification, NotificationType } from '../model/notification';   
import { HTTPError } from '../model/error';
import { RegisterPopup } from './registerPopup';

type NotificationManagerProps = {
    context : AppContext
}

type NotificationManagerState = {
    notifications : Array<Notification>
}

export class NotificationManager extends React.Component<NotificationManagerProps, NotificationManagerState> {

    constructor(props) {
        super(props);

        this.state = {
            notifications: []
        };

        this.handleOnCloseNotification = this.handleOnCloseNotification.bind(this);
        this.defaultHTTPCatch = this.defaultHTTPCatch.bind(this);
    }

    addNotification(notification : Notification) {
        if(notification.autoClose === undefined || notification.autoClose) {
            setTimeout(() => {
                var na = this.state.notifications;
                na.splice(na.indexOf(notification), 1);
                this.setState({
                    notifications: na
                });
            }, notification.autoCloseTimeout || 2500);
        }
        this.setState({
            notifications: [notification].concat(this.state.notifications)
        });
    }

    defaultHTTPCatch(ex : HTTPError) {
        this.addNotification({
            type: NotificationType.danger,
            content: <div><strong>Error 🤦!</strong> { ex.errorMessage } 🤷‍</div>
        });
    }

    handleOnCloseNotification(i : number) {
        var na = this.state.notifications;
        na.splice(i, 1);
        this.setState({
            notifications: na
        });
    }

    render() {
        return <div className="container fixed-bottom" id="notifications">
            { this.state.notifications.map((n, i) => <NotificationComponent key={ i } context={ this.props.context } onClose={ () => this.handleOnCloseNotification(i) } notification={ n }></NotificationComponent>) }
        </div>;
    }
};
