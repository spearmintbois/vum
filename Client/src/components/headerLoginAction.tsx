import * as React from 'react';
import { Link } from 'react-router-dom'

import { AppContext } from '../model/appContext';

type HeaderLoginActionProps = {
    context : AppContext
}

export const HeaderLoginAction : React.StatelessComponent<HeaderLoginActionProps> = (props) => {
    return <ul className="nav navbar-nav">
        <li className="nav-item">
            <div className="btn btn-header btn-login">
                <Link to="/login">Sign in</Link>
            </div>
        </li>
        <li className="nav-item">
            <div className="btn btn-header btn-register">
                <Link to="/register">I'm new here</Link>
            </div>
        </li>
    </ul>;
}
