import * as React from 'react';

type ReactionPanelProps = {
    upvotes : number;
    downvotes : number;
    onUpVote? : () => void;
    onDownVote? : () => void;
};

export const ReactionPanel : React.StatelessComponent<ReactionPanelProps> = (props) => {
    var textUpVote = (props.upvotes != 0 ? props.upvotes + " " : "") + "❤",
        textDownVote = (props.downvotes != 0 ? props.downvotes + " " : "") + "💩";
    return <div>
        <button 
            type="button"
            onClick={ () => props.onUpVote } 
            className="btn btn-light btn-vote">{ textUpVote }</button>
        <button 
            type="button"
            onClick={ () => props.onDownVote } 
            className="btn btn-light btn-vote">{ textDownVote }</button>
    </div>;
};
