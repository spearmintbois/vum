import * as React from 'react';
import { AppContext } from '../model/appContext';
import { User } from '../model/user';
import { Avatar } from './avatar';

type AdminUsersOverviewProps = {
    context : AppContext
    user : User
    handleDeleteUser : (userName : String) => void
}

export const AdminUsersOverviewComponent : React.StatelessComponent<AdminUsersOverviewProps> = (props) => {

    return <div className="col-3" id="feed">
       <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="d-flex justify-content-between">
                                <div className="col-2">
                                    <Avatar user={ props.user.username } showName={ true } />
                                    <p>{ props.user.email }</p>
                                </div>
                                <div>
                                    <button 
                                        onClick={() => props.handleDeleteUser(props.user.username)} 
                                        type="button" 
                                        className="btn btn-danger">
                                        X
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>;
    
}