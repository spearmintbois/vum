import * as React from 'react';
import { History } from 'history';
import { withRouter } from "react-router";

import { Backend } from '../backend';
import { AppContext } from '../model/appContext';
import { HTTPError } from '../model/error';
import { NotificationType } from '../model/notification';

type LoginPopupProps = {
    context : AppContext
    history : History
}

type LoginPopupState = {
    username : string,
    password : string
}

class Login extends React.Component<LoginPopupProps, LoginPopupState> {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: ''
        }

        this.handleClose = this.handleClose.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleOnChangePassword = this.handleOnChangePassword.bind(this);
        this.handleOnChangeUsername = this.handleOnChangeUsername.bind(this);
    }

    handleClose() {
        this.props.history.push({
            pathname: '/'
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        event.stopPropagation();

        Backend.login(this.state.username, this.state.password).then((result) => {
            if(result.success) {
                this.props.context.onLoginChange(result.user);
                this.handleClose();
            }
        }).catch(this.props.context.notificationManagerRef.current.defaultHTTPCatch);
    }

    handleOnChangeUsername(event) {
        this.setState({
            username: event.target.value
        });
    }

    handleOnChangePassword(event) {
        this.setState({
            password: event.target.value
        });
    }

    render() {
        return <form onSubmit={ this.handleSubmit }>
            <div className="modal fade show" role="dialog" style={{ display: 'block' }}>
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Welcome back! ❤</h5>
                        </div>
                        <div className="modal-body">
                            <p>
                                Login with your username and password 👌. If you lost one of them, bad for you, because we are too lazy to implement a account recovery system 🤷‍.
                            </p>
                            <div className="form-group">
                                <input type="text" className="form-control" placeholder="Username" onChange={ this.handleOnChangeUsername } required />
                                {
                                    this.state.username == "" ? <div className="invalid-feedback" style={{ display: 'block' }}>
                                    We need your username to know who you are.
                                    </div> : null
                                }                            
                            </div>
                            <div className="form-group">
                                <input type="password" className="form-control" placeholder="Password" onChange={ this.handleOnChangePassword } required />
                                {
                                    this.state.password == "" ? <div className="invalid-feedback" style={{ display: 'block' }}>
                                    Please enter your Password. Don't worry, we will never share this 🤫.
                                    </div> : null
                                }
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={ this.handleClose }>Close</button>
                            <button type="submit" className="btn btn-primary btn-block">Login</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal-backdrop show" />
        </form>;
    }
}

export const LoginPopup = withRouter(Login);
