import * as React from 'react';
import { Route } from "react-router-dom";

import { AppContext } from '../model/appContext';
import { Post } from '../model/post';
import { PostComponent } from './post';
import { Backend } from '../backend';
import { Socket } from '../socket';
import { VoteResponse } from '../model/voteResponse';

import { LoginPopup } from './loginPopup';
import { RegisterPopup } from './registerPopup';
import { PostPopup } from './postingPopup';

type FeedProps = {
    context : AppContext
}

type FeedState = {
    posts : Post[]
}

const postPreloadCount = 5;
const postLoadCount = 1;

export class Feed extends React.Component<FeedProps, FeedState> {
    private socket : Socket;

    private postOffset = 0;
    private isLoading = false;

    constructor(props) {
        super(props);

        this.state = {
            posts: []
        }; 

        this.handleVotes = this.handleVotes.bind(this);
        this.handleOnScroll = this.handleOnScroll.bind(this);
    }

    getPosts(howMany : number) : Promise<Array<Post>> {
        var promise = Backend.getFeed(this.postOffset, howMany);
        this.postOffset += howMany;
        return promise;
    }

    componentDidMount() {
        this.getPosts(postPreloadCount).then((posts) => {
            this.setState({
                posts: posts
            })
        });
        window.addEventListener('scroll', this.handleOnScroll);
        this.socket = new Socket();
        this.socket.onNewPost = (post) => {
            this.handleNewPost(post);
        };
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleOnScroll);
        this.socket.close();
    }

    handleNewPost(post : Post) {
        this.postOffset += 1;
        this.setState({
            posts: [post].concat(this.state.posts)
        });
    }

    handleVotes(mode : boolean, postId : number){
        let prom;
        if(mode){
            prom = Backend.postVoteUp(postId);
        }else{
            prom = Backend.postVoteDown(postId);
        }
        prom.then((res : VoteResponse) => {
            let newposts = this.state.posts.map((element) => {
                if(element.id == postId){
                    element.voteBalance = res.voteBalance;
                    element.upVotes = res.upVotes;
                    element.downVotes = res.downVotes;
                }
                return element;
            });

            this.setState({
                posts: newposts
            })
        });
    }

    handleOnScroll() {
        if(window.innerHeight + window.scrollY >= document.body.offsetHeight - 500 && !this.isLoading) {
            // Scrolled to bikini bottim
            this.isLoading = true;
            this.getPosts(postLoadCount).then((posts) => {
                this.setState({
                    posts: this.state.posts.concat(posts)
                });
                this.isLoading = false;
            });
        }
    }

    render() {
        return <div className="container" id="feed">
            { this.state.posts.map(p => <PostComponent key={ p.id } context={ this.props.context } post={ p } handleVotes={ this.handleVotes } />) }
        </div>;
    }
}