import * as React from 'react';
import { BrowserRouter, Route } from "react-router-dom";
import { User } from '../model/user';
import { AppContext } from '../model/appContext';
import { Header } from './header';
import { Feed } from './feed';
import { Settings } from './settings';
import { LoginPopup } from './loginPopup';
import { RegisterPopup } from './registerPopup';
import { PostPopup } from './postingPopup';
import { AdminDashboard } from './adminDashboard';
import { NotificationManager } from './notificationManager';
import { NotificationType, Notification } from '../model/notification';
import { Profile } from './profile';


type AppProps = {
    
}

type AppState = {
    loggedInUser? : User,
    isAdmin: boolean
}

const routes = [{
    path: "/",
    exact: true,
    page: Feed
},
{
    path: "/login",
    page: Feed,
    popup: LoginPopup
},
{
    path: "/register",
    page: Feed,
    popup: RegisterPopup
},
{
    path: "/post",
    page: Feed,
    popup: PostPopup
},
{
    path: "/settings",
    page: Settings
},
{
    path: "/admin",
    page: AdminDashboard
},
{
    path: "/profile/:name",
    page: ({ context, match }) => <Profile context={ context } name={ match.params.name } />
}];

export class App extends React.Component<AppProps, AppState> {
    notificationManagerRef = React.createRef<NotificationManager>();

    constructor(props) {
        super(props);

        this.state = {
            loggedInUser: null,
            isAdmin: false // Default false
        };

        this.handleOnLoginChange = this.handleOnLoginChange.bind(this);
    }

    componentDidMount() {
        this.notificationManagerRef.current.addNotification({
            type: NotificationType.primary,
            content: <div><strong>Welcome!</strong> to the best Meme's evver. 😁</div>
        });
    }

    handleOnLoginChange(user? : User) {
        this.setState({
            loggedInUser: user
            //TODO Check that user is admin
        });
    }

    generateContext() : AppContext {
        return {
            onLoginChange: this.handleOnLoginChange,
            isLoggedIn: this.state.loggedInUser != null,
            loggedInUser: this.state.loggedInUser,
            isAdmin: this.state.isAdmin,
            notificationManagerRef: this.notificationManagerRef
        };
    }

    render() {
        let ctx = this.generateContext();
        let wienerliImTeig = (Wienerli, senf) => {
            if(Wienerli == undefined) return null;
            return <Wienerli context={ ctx } { ...senf } />
        };
        return <BrowserRouter>
            <div>
                <Header context={ ctx }></Header>
                { routes.map((r, i) => <Route key={ i } path={ r.path } exact={ r.exact } component={ (senf) => wienerliImTeig(r.popup, senf) } />) }
                { routes.map((r, i) => <Route key={ i } path={ r.path } exact={ r.exact } component={ (senf) => wienerliImTeig(r.page, senf) } />) }
                <NotificationManager ref={ this.notificationManagerRef } context={ ctx }></NotificationManager>
            </div>
        </BrowserRouter>;
    }
}
