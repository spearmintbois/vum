import * as React from 'react';
import { History } from 'history';
import { withRouter } from "react-router";

import { Backend } from '../backend';
import { AppContext } from '../model/appContext';

type RegisterPopupProps = {
    context : AppContext
    history : History
}

type RegisterPopupState = {
    username : string,
    password : string
    email : string
}

class Register extends React.Component<RegisterPopupProps, RegisterPopupState> {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            email: ''
        }

        this.handleClose = this.handleClose.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleOnChangeUsername = this.handleOnChangeUsername.bind(this);
        this.handleOnChangePassword = this.handleOnChangePassword.bind(this);
        this.handleOnChangeEmail = this.handleOnChangeEmail.bind(this);
    }

    handleClose() {
        this.props.history.push({
            pathname: '/'
        });
    }

    async handleSubmit(event) {
        event.preventDefault();
        event.stopPropagation();
        
        await Backend.register(this.state.username, this.state.email, this.state.password);
        let result;
        try {
            result = await Backend.login(this.state.username, this.state.password);
        } catch(ex) {
            this.props.context.notificationManagerRef.current.defaultHTTPCatch(ex);
        }

        if(result.success) {
            this.props.context.onLoginChange(result.user);
            this.handleClose();
        }
    }

    handleOnChangeUsername(event) {
        this.setState({
            username: event.target.value
        });
    }

    handleOnChangePassword(event) {
        this.setState({
            password: event.target.value
        });
    }

    handleOnChangeEmail(event) {
        this.setState({
            email: event.target.value
        });
    }

    render() {
        return <form onSubmit={ this.handleSubmit }>
            <div className="modal fade show" role="dialog" style={{ display: 'block' }}>
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Newbie...huh?</h5>
                        </div>
                        <div className="modal-body">
                            <p>
                                Register to the BEST meme community on this planet.
                            </p>
                            <div className="form-group">
                                <input type="text" className="form-control" placeholder="Username" onChange={ this.handleOnChangeUsername } required />  
                            </div>
                            <div className="form-group">
                                <input type="password" className="form-control" placeholder="Password" onChange={ this.handleOnChangePassword } required />  
                            </div>                            
                            <div className="form-group">
                                <input type="email" className="form-control" placeholder="Email" onChange={ this.handleOnChangeEmail } required />  
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={ this.handleClose }>Close</button>
                            <button type="submit" className="btn btn-primary btn-block">Join the dark side</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal-backdrop show" />
        </form>;
    }
}

export const RegisterPopup = withRouter(Register);
