import * as React from 'react';
import { AppContext } from '../model/appContext';
import { Post } from '../model/post';
import { Backend } from '../backend';
import { Avatar } from './avatar';
import { ReactionPanel } from './reactionPanel';
import { Link } from "react-router-dom";

type PostProps = {
    context : AppContext
    post : Post
    handleVotes : (mode, postId) => void
}

export const PostComponent : React.StatelessComponent<PostProps> = (props) => {
    return  <div className="row post">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="d-flex justify-content-between">
                                <div className="col-2">
                                    <Link to={ "profile/" + props.post.posterName }>
                                        <Avatar user={ props.post.posterName } showName={ true } />
                                    </Link>
                                </div>
                                <div>
                                    <ReactionPanel 
                                        upvotes={ props.post.upVotes } 
                                        downvotes={ props.post.downVotes }
                                        onUpVote={ () => props.handleVotes(true, props.post.id) }
                                        onDownVote={ () => props.handleVotes(false, props.post.id) } />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <h3>{ props.post.title }</h3>
                    </div>           
                    <div className="row">
                        { props.post.hasAttachment ? <img className="img-fluid post-attachment" src={ props.post.attachmentUrl } /> : null }                            
                    </div>
                    <div className="row">
                        { props.post.hasContent ? <p>{ props.post.content }</p> : null }    
                    </div>
                </div>
            </div>;
};
