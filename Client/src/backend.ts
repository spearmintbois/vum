import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { User } from './model/user';
import { AuthResultDTO } from './model/DTO/authResult';
import { AuthResult } from './model/authResult';
import { Post } from './model/post';
import { UserDTO } from './model/DTO/user';
import { VoteResponse } from './model/voteResponse';
import { HTTPError } from './model/error';

export class Backend {
    private static host = (window.location.protocol == 'https:' ? 'https' : 'http') + '://' + window.location.host;
    private static API = Backend.host + '/api';
    private static token : string;

    private static getConfig() : AxiosRequestConfig {
        return {
            headers: { 'Authorization': this.token != null ? 'Bearer ' + this.token : null }
        };
    }

    private static getHTTPError(response : AxiosResponse) : HTTPError {
        var message;
        if(typeof response.data === 'string') {
            message = response.data as string;
        }
        return {
            statusCode: response.status,
            statusText: response.statusText,
            errorMessage: message
        };
    }

    static async get<T>(route : string) {
        let response = await axios.get<T>(this.API + route, this.getConfig());
        if(response.status != 200 && response.status != 204) {
            throw this.getHTTPError(response);
        } else {
            return response.data;
        }
    }

    static async post<T>(route : string, data?) : Promise<T> {
        let response = await axios.post<T>(this.API + route, data, this.getConfig());
        if(response.status != 200 && response.status != 204) {
            throw this.getHTTPError(response);
        } else {
            return response.data;
        }
    }

    static async put<T>(route : string, data?) : Promise<T> {
        let response = await axios.put<T>(this.API + route, data, this.getConfig());
        if(response.status != 200 && response.status != 204) {
            throw this.getHTTPError(response);
        } else {
            return response.data;
        }
    }

    static async delete<T>(route : string) : Promise<T> {
        let response = await axios.delete(this.API + route, this.getConfig());
        if(response.status != 200 && response.status != 204) {
            throw this.getHTTPError(response);
        } else {
            return response.data;
        }
    }

    static async login(username, password) : Promise<AuthResult> {
        let response = await this.post<AuthResultDTO>('/security/authenticate', {
            username: username,
            password: password
        });
        if(response.success) {
            this.token = response.token;
        }
        return AuthResult.fromDTO(response);
    }

    static async deletePost(postId: number) {
        return await this.delete('/post/delete/' + postId);
    }

    static async deleteUser(userEmail : String): Promise<UserDTO> {
        return await this.delete<UserDTO>('/user/' + userEmail);
    }

    static async loginTest() : Promise<string> {
        return this.get<string>('/security/authenticate');
    }

    static async getPosts() : Promise<Array<Post>> {
        return this.get<Array<Post>>('/posts');
    }

    static async getUsers() : Promise<Array<User>> {
        return this.get<Array<User>>('/users');
    }
    
    static async getFeed(offset=0, limit=10) : Promise<Array<Post>> {
        return this.get<Array<Post>>('/post/feed?offset=' + offset + '&limit=' + limit);
    }

    static async register(username, email, password) : Promise<User> {
        let response = await this.post<UserDTO>('/user', {
            username: username,
            email: email,
            password: password
        });
        return User.formDTO(response);
    }

    static async uploadPost(title : string, content? : string, attachment? : File) {
        let data = new FormData();
        data.set('post', JSON.stringify({
            title: title,
            content: content
        }));
        if(attachment != null) {
            data.set('attachment', attachment);
        }
        return await axios({
            method: 'post',
            url: this.API + '/post',
            data: data,
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + this.token
            }
        });
    }

    static async updateUser(email : string, currentPassword : string, newPassword : string, isBanned? : boolean) {
        return this.put<any>('/user', {
            email: email,
            password: currentPassword,
            newPassword: newPassword,
            isBanned: isBanned
        });
    }

    static async updateUserAvatar(avatar : File) {
        let data = new FormData();
        data.set('avatar', avatar);
        return await axios({
            method: 'put',
            url: this.API + '/user/avatar',
            data: data,
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + this.token
            }
        });
    }

    static async postVoteUp(postId: number) : Promise<VoteResponse> {
        return this.post<VoteResponse>('/post/'+ postId +'/vote/up');
    }

    static async postVoteDown(postId: number) : Promise<VoteResponse> {
        return this.post<VoteResponse>('/post/'+ postId +'/vote/down');
    }

    static getAvatarUrl(name : string) : string {
        return this.API + "/user/" + name + "/avatar";
    }

    static getPostsFrom(name : string) : Promise<Array<Post>> {
        return this.get<Array<Post>>('/user/' + name + '/posts');
    }
}
