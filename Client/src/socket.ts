import { Post } from "./model/post";

export class Socket {
    private websocket;

    constructor() {
        this.onopen = this.onopen.bind(this);
        this.onclose = this.onclose.bind(this);
        this.onmessage = this.onmessage.bind(this);
        this.connect();
    }

    connect() {
        this.websocket = new WebSocket((window.location.protocol == 'https:' ? 'wss' : 'ws') + '://' + window.location.host + '/api/feed/');
        this.websocket.onopen = this.onopen;
        this.websocket.onclose = this.onclose;
        this.websocket.onmessage = this.onmessage;
    }

    close() {
        this.websocket.close();
    }

    onopen() {
        console.log("Connected to websocket!");
    }

    onclose() {
        console.log("Disconnected from websocket!");
        setTimeout(() => {
            console.log("Trying to reconnect...");
            this.connect();
        }, 1000);
    }

    onmessage(msg) {
        // Currently we only recive NewPostEvents from the server. 
        // The message is therefore allways a PostDTO Object.
        var data = JSON.parse(msg.data);
        this.onNewPost({
            id: data.Id,
            attachmentUrl: data.AttachmentUrl,
            content: data.Content,
            hasAttachment: data.HasAttachment,
            hasContent: data.HasContent,
            poster: {
                active: data.Poster.Active,
                created: new Date(Date.parse(data.Poster.Created)),
                email: data.Poster.Email,
                username: data.Poster.Username
            },
            posterName: data.PosterName,
            title: data.Title,
            voteBalance: data.VoteBalance,
            upVotes: data.UpVotes,
            downVotes: data.DownVotes
        });
    }

    onNewPost : (post : Post) => void 
}
