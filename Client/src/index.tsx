import * as React from 'react';
import * as ReactDOM from 'react-dom';

import './styles.less';
import { App } from './components/app';

ReactDOM.render(<App />, document.getElementById('app'));
