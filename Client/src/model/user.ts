import { UserDTO } from "./DTO/user";

export class User {
    username : string
    email : string
    created : Date
    active : boolean

    static formDTO(dto : UserDTO) : User {
        return {
            username: dto.username,
            email: dto.email,
            created: new Date(Date.parse(dto.created)),
            active: dto.active
        };
    }
}