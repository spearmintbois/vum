import { User } from "./user";

export class Comment {
    id : number
    content : string
    postedAt : Date
    poster : User
    replyTo : Comment
}