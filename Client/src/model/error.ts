
export class HTTPError {
    statusCode : number;
    statusText : string;
    errorMessage : string;
}
