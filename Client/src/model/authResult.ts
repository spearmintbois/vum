import { User } from "./user";
import { AuthResultDTO } from "./DTO/authResult";

export class AuthResult {
    success : boolean
    user : User
    token : string

    static fromDTO(dto : AuthResultDTO) {
        return {
            success: dto.success,
            user: User.formDTO(dto.user),
            token: dto.token
        };
    }
}