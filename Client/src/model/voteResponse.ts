export class VoteResponse {
    voteBalance: number
    upVotes: number
    downVotes: number
}