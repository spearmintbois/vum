
export enum NotificationType {
    primary = "alert-primary",
    secondary = "alert-secondary",
    success = "alert-success",
    info = "alert-info",
    warning = "alert-warning",
    danger = "alert-danger",
    light = "alert-light",
    dark = "alert-dark"
}

export class Notification {
    type? : NotificationType
    content : JSX.Element
    autoClose? : boolean
    autoCloseTimeout? : number
}
