import { User } from "./user"; 
import { NotificationManager } from "../components/notificationManager";

export interface AppContext {
    onLoginChange : (user? : User) => void
    isLoggedIn : boolean
    loggedInUser? : User
    isAdmin : boolean
    notificationManagerRef : React.RefObject<NotificationManager>
}