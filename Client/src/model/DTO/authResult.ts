import { UserDTO } from "./user";

export class AuthResultDTO {
    success : boolean
    user : UserDTO
    token : string
}
