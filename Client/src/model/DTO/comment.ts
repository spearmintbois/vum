import { UserDTO } from "./user";

export class CommentDTO {
    id : number
    content : string
    postedAt : string
    poster : UserDTO
    replyTo : Comment
}