import { User } from "./user";

export class Post {
        id : number
        title : string
        content : string
        attachmentUrl : string
        hasContent : boolean
        hasAttachment : boolean
        posterName : string
        poster : User
        voteBalance : number
        upVotes : number
        downVotes : number
}