using System;
using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Api.Events
{
    public class NewPostEvent : EventArgs
    {
        public Post Post { get; set; }
    }
}