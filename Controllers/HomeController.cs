using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace vum.Controllers
{
    public class HomeController : ControllerBase
    {
        public IActionResult Index() {
            return File("~/index.html", "text/html");
        }
    }
}