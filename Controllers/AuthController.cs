using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using Spearmint.Vum.Backend.Api.Models;
using Spearmint.Vum.Backend.Api.Services;

namespace Spearmint.Vum.Backend.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/security")]
    public class AuthController : ControllerBase
    {
        private IAuthService auth;

        public AuthController(IAuthService auth) {
            this.auth = auth;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] AuthParam param) {
            AuthResult result = auth.Authenticate(param.Username, param.Password);

            if(!result.Success) {
                return Unauthorized("Username or Password is incorrect");
            }
            return Ok(result);
        }

        [HttpGet("test")]
        public IActionResult Test() {
            var user = auth.GetUserFromPrincipal(this.User);
            return Ok("Welcome " + user.Email + "!");
        }

        [HttpGet("current")]
        public Models.DTOs.User GetCurrentUser() {
            return new Models.DTOs.User(auth.GetUserFromPrincipal(this.User));
        }
    }
}