using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Spearmint.Vum.Backend.Api.Services;

namespace vum.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/security")]
    public class PermissionController : ControllerBase
    {
        private IAuthService auth;

        public PermissionController(IAuthService auth) {
            this.auth = auth;
        }

        [AllowAnonymous]
        [HttpGet("permissions")]
        public IActionResult GetPermissions() {
            var user = auth.GetUserFromPrincipal(this.User);
            if(user == null) {
                return Ok(new string[0]);
            }
            return Ok(user.Role.RolePermissions.Select(p => p.Permission.Name).ToArray());
        }
    }
}