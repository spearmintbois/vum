using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Options;
using Spearmint.Vum.Backend.Api.Helpers;
using Spearmint.Vum.Backend.Api.Models;
using Spearmint.Vum.Backend.Api.Models.DTOs;
using Spearmint.Vum.Backend.Api.Services;
using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/post")]
    [Route("api/posts")]
    public class PostController : ControllerBase
    {
        private readonly IRepositoryService repositoryService;
        private readonly IAuthService authService;
        private readonly IEventService eventService;
        private readonly AppSettings appSettings;


        public PostController(IRepositoryService repositoryService, IAuthService authService, IEventService eventService, IOptions<AppSettings> appSettings) {
            this.repositoryService = repositoryService;
            this.authService = authService;
            this.eventService = eventService;
            this.appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<Models.DTOs.Post> GetPosts() {
            return repositoryService.PostRepository.GetAll().Select(p => new Models.DTOs.Post(p)).ToList();
        }

        [AllowAnonymous]
        [HttpGet("feed")]
        public IEnumerable<Models.DTOs.Post> GetFeed([FromQuery] int offset=0, [FromQuery] int limit=10) {
            return repositoryService.PostRepository.GetAll()
                .OrderByDescending(p => p.PostedAt)
                .Skip(offset)
                .Take(limit)
                .Select(p => new Models.DTOs.Post(p)).ToList();
        }

        [HttpPost, DisableRequestSizeLimit]
        public IActionResult UploadPost([ModelBinder(BinderType = typeof(JsonModelBinder))] PostUploadParam post, [FromForm] IFormFile attachment) {
            var currentUser = this.authService.GetUserFromPrincipal(this.User);
            Model.Entities.Post newPost = new Model.Entities.Post {
                Title = post.Title,
                Content = post.Content,
                PosterId = currentUser.Id,
                Poster = currentUser
            };
            if(attachment != null && attachment.Length > 0) {
                if(!appSettings.MIMETypeWhitelist.Contains(attachment.ContentType)) {
                    newPost = null;
                    return BadRequest("ContentType not supported!");
                }
                newPost.AttachmentFilename = attachment.FileName;
                newPost.AttachmentMIMEType = attachment.ContentType;
                using(var stream = new MemoryStream()) {
                    // TODO: Make this async
                    attachment.CopyTo(stream);
                    newPost.Attachment = stream.ToArray();
                }
            }
            repositoryService.PostRepository.CreatePost(newPost);
            eventService.RaiseNewPostEvent(this, new Events.NewPostEvent {
                Post = newPost
            });
            return CreatedAtAction("UploadPost", new Models.DTOs.Post(newPost));
        }

        [AllowAnonymous]
        [ResponseCache(Duration = 60 * 60 * 24)]
        [HttpGet("{id}/attachment")]
        public IActionResult GetAttachment(int id) {
            Model.Entities.Post post = repositoryService.PostRepository.Get(id);
            if(post.Attachment == null) {
                return BadRequest("Post has no attachment!");
            }
            return File(post.Attachment, post.AttachmentMIMEType);
        }

        [HttpPost("{id}/vote/up")]
        public IActionResult UpVotePost(int id) {
            var user = authService.GetUserFromPrincipal(this.User);
            var post = repositoryService.PostRepository.Get(id);
            if(post == null) {
                return NotFound();
            }
            repositoryService.VoteRepository.UpVotePost(post, user);
            return Ok(new VoteResponse() {
                VoteBalance = post.VoteBalance,
                DownVotes = post.DownVotes,
                UpVotes = post.UpVotes
            });
        }

        [HttpPost("{id}/vote/down")]
        public IActionResult DownVotePost(int id) {
            var user = authService.GetUserFromPrincipal(this.User);
            var post = repositoryService.PostRepository.Get(id);
            if(post == null) {
                return NotFound();
            }
            repositoryService.VoteRepository.DownVotePost(post, user);
            return Ok(new VoteResponse() {
                VoteBalance = post.VoteBalance,
                DownVotes = post.DownVotes,
                UpVotes = post.UpVotes
            });
        }

        [HttpDelete("{id}/vote")]
        public IActionResult DeleteVote(int id) {
            var user = authService.GetUserFromPrincipal(this.User);
            var post = repositoryService.PostRepository.Get(id);
            repositoryService.VoteRepository.RemoveVoteFromUser(post, user);
            return Ok(new VoteResponse() {
                VoteBalance = post.VoteBalance,
                DownVotes = post.DownVotes,
                UpVotes = post.UpVotes
            });
        }

        [HttpDelete("delete/{id}")]
        public IActionResult DeletePost(int id) {
            var postWasDeleted = repositoryService.PostRepository.DeletePost(id);
            //TODO return internal server error if delete failed
            return NoContent();
        }
    }
}