using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

using Spearmint.Vum.Backend.Api.Models;
using Spearmint.Vum.Backend.Api.Models.DTOs;
using Spearmint.Vum.Backend.Api.Services;
using Spearmint.Vum.Backend.Model.Entities;

namespace Spearmint.Vum.Backend.Api.Controllers
{

    [ApiController]
    [Route("api/user")]
    [Route("api/users")]
    public class UserController : ControllerBase
    {
        private readonly IRepositoryService repositoryService;
        private readonly IAuthService authService;
        private readonly ISettingService settingService;

        public UserController(IRepositoryService repositoryService, IAuthService authService, ISettingService settingService) {
            this.repositoryService = repositoryService;
            this.authService = authService;
            this.settingService = settingService;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Register([FromBody] RegisterParam param) {
            var user = repositoryService.UserRepository.CreateUser(param.Username, param.Email, param.Password, settingService.RoleId);
            if(user == null) {
                return BadRequest();
            }
            return CreatedAtAction("register", user);
        }

        [HttpPut]
        public IActionResult Update([FromBody] UserUpdateParam param) {
            var user = authService.GetUserFromPrincipal(this.User);            
            if(param.NewPassword != null) {
                if(user.Password != param.Password) {
                    return BadRequest("Password mismatch!");
                }
                user.Password = param.NewPassword;
            }
            user.Email = param.Email;
            repositoryService.Context.Users.Update(user);
            repositoryService.Context.SaveChanges();
            return NoContent();
        }

        [HttpPut("avatar")]
        public IActionResult UpdateAvatar([FromForm] IFormFile avatar) {
            var user = authService.GetUserFromPrincipal(this.User);
            if(avatar == null && avatar.Length <= 0) {
                return BadRequest("No image attached :(");
            }
            if(avatar.ContentType != "image/jpeg") {
                return BadRequest("Only image/jpeg files are accepted!");
            }
            using(var stream = new MemoryStream()) {
                // TODO: Make this async
                avatar.CopyTo(stream);
                user.Avatar = stream.ToArray();
            }
            repositoryService.Context.Users.Update(user);
            repositoryService.Context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{userName}")]
        public IActionResult DeleteUser(string userName) {

            if(userName != null && userName.Length > 0) {
                var user = repositoryService.UserRepository.Find(userName);
                repositoryService.UserRepository.Delete(user.Id);
                return NoContent();
            }

            return BadRequest("Invalid user");
        }


        [HttpGet]
        public IActionResult GetUsers() {
            var users = repositoryService.UserRepository.GetAll();
            if(users == null) {
                return NotFound();
            }
            return Ok(users);
        }


        [AllowAnonymous]
        [HttpGet("{name}")]
        public IActionResult GetUser(string name) {
            var user = repositoryService.UserRepository.Find(name);
            if(user == null) {
                return NotFound();
            }
            return Ok(new Api.Models.DTOs.User(user));
        }

        [AllowAnonymous]
        [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
        [HttpGet("{name}/avatar")]
        public IActionResult GetAvatar(string name) {
            var user = repositoryService.UserRepository.Find(name);
            if(user == null) {
                return NotFound();
            }
            if(user.Avatar == null) {
                return File("~/assets/pool.jpg", "image/jpeg");
            }
            return File(user.Avatar, "image/jpeg");
        }

        [AllowAnonymous]
        [HttpGet("{name}/posts")]
        public IActionResult GetPostsFrom(string name) {
            var user = repositoryService.UserRepository.Find(name);
            if(user == null) {
                return NotFound();
            }
            
            var posts = repositoryService.PostRepository.GetAll().Where(p => p.PosterId == user.Id).ToList();
            if(posts.Count <= 0) {
                return NoContent();
            }

            return Ok(posts.Select(p => new Models.DTOs.Post(p)).ToArray());
        }
    }
}