using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Spearmint.Vum.Backend.Api.Models.DTOs;
using Spearmint.Vum.Backend.Api.Services;

namespace vum.Controllers
{
    [ApiController]
    [Route("api/post/{id}")]
    public class CommentController : ControllerBase
    {
        private IAuthService auth;
        private IRepositoryService repository;

        public CommentController(IAuthService auth, IRepositoryService repository) {
            this.auth = auth;
            this.repository = repository;
        }

        [HttpGet("comments")]
        public IActionResult PostComments(int postId) {
            var post = repository.PostRepository.Get(postId);
            if(post == null) {
                return NotFound();
            }
            if(post.Comments == null) {
                return NoContent();
            }

            return Ok(post.Comments.Select(c => new Comment(c)).ToArray());
        }
    }
}