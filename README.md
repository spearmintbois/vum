# vum-backend-api
6er BOIII!!!!

## Docker

### All components

Including:
- MySql as Database
- Nginx as Proxy
- and vum

Checkout via git clone and build images

```
docker-compose build
```

Start all components

```
docker-compose up -d
```

Stop all components

```
docker-compose down
```

### Migrate database

Currently the migration does not happen automaticly on the database. To solve that issue you have to run the following command on you local machine.

```
dotnet ef migrations script --idempotent
```

This creates a sql script to migrate a database. Then execute this sql on the docker mysql server.


### Only App
```
docker build -t spearmintbois/vum:1.0 .
docker run --name vum -d -p 80:80 spearmintbois/vum:1.0
```

The app will then listen on the port specified, per default on 80.


## Endpoints

### /api/security/authenticate
Accepts POST request with following body:
```json
{
    "username": "xy",
    "password": "1234"
}
```
Returns AuthResult Object if successfully logged in:
```json
{
    "success": true,
    "user": {
        "username": "aaron",
        "email": "aaron.schmid@outlook.com",
        "created": "2018-12-14T12:31:12.7266883",
        "active": true
    },
    "token": "jwt token..."
}
```
or status code 400 if not successful with the following body:
```json
{
    "success": false,
    "message": "Username or Password is incorrect"
}
```

### /api/security/test
Accepts GET, if you are logged in it returns `Welcome {email}!` with your current acc email. If you are not logged in => status code 401.

### /api/security/current
Accepts GET, if you are logged in the current user dto object is return.
```json
{
    "username": "aaron",
    "email": "aaron.schmid@outlook.com",
    "created": "2018-12-14T12:31:12.7266883",
    "active": true
}
```
If not, status code 401.

### /api/posts
Accepts GET, returns all posts currently in data base.
```json
[
    {
        "id": 1,
        "title": "test",
        "content": "Hello World!",
        "attachmentUrl": "/api/post/1/attachment",
        "hasContent": true,
        "hasAttachment": true,
        "posterName": "aaron",
        "poster": {
            "username": "aaron",
            "email": "aaron.schmid@outlook.com",
            "created": "2018-12-18T09:01:36.9494679",
            "active": true
        }
    }
]
```

### /api/post/{id}/attachment
Accepts GET, return the attachment for the specified post.

### /api/post
Accepts POST, Content-Type multipart/fo rm-data, requires the following body structure.

| Key        | Value                                                      |
| ---------- | ---------------------------------------------------------- |
| post       | Plain-Text: { "title": "Hello World!", "content": "Test" } |
| attachment | File                                                       |

Content and attachment are optional, but one of them is required.
