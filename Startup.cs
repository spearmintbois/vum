﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Spearmint.Vum.Backend.Api.Helpers;
using Spearmint.Vum.Backend.Api.Services;
using Spearmint.Vum.Backend.Model;
using Microsoft.EntityFrameworkCore;
using Spearmint.Vum.Backend.Api.Middlewares;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.Net.Http.Headers;

namespace Spearmint.Vum.Backend.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc();

            // configure settings for app
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettings>();

            // configure Database Context
            services.AddDbContext<VumContext>(options => options
                .UseLazyLoadingProxies()
                .UseMySql(Configuration.GetConnectionString("VumDatabase")));

            // configure services for injection
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IRepositoryService, RepositoryService>();
            services.AddSingleton<IEventService, EventService>();
            services.AddScoped<ISettingService, SettingService>();
            
            // configure jwt authentication
            services.AddAuthentication(x => {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x => {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(appSettings.Secret)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseWebSockets();
            app.UseMiddleware<FeedMiddleware>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            //app.UseDefaultFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = (ctx) => {
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] = "public,max-age=" + (60 * 60);
                }
            });

            if(env.IsDevelopment()) {
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "Client", "dist")),
                    RequestPath = "/js"
                });
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();

            app.MapWhen(context => !context.Request.Path.Value.StartsWith("/api"), builder => {
                builder.UseMvc(routes => {
                    routes.MapSpaFallbackRoute("sap-fallback", new { controller = "Home", action = "Index" });
                });
            });
            app.UseMvc();
        }
    }
}
